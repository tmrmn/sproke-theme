# Sproke #

This is a fork of the beautiful Soda Theme by Ian Hill (https://github.com/buymeasoda/soda-theme)
Please visit his repository for more information.

##  About #

I really liked the Soda Dark theme but found it not really fitting with the Ubuntu Radiance theme, so I mixed the light and dark variant together and tinted the light UI elements to blend in with the Radiance colors (plus some additional changes)

![Soda Hybrid Theme](http://i.imgur.com/Da60Ycc.png)

## Sproke? #

Jup, Sproke, Coke and Sprite mixed together.

## License

Based on Soda Theme by Ian Hill (http://buymeasoda.com/), licensed under the [Creative Commons Attribution-ShareAlike 3.0 License](http://creativecommons.org/licenses/by-sa/3.0/).
